# Example Groovy/JPA Application

## Creation

This project was created from the command line with the command:

`mn create-app -l=groovy -f=data-hibernate-jpa -i uk.co.channele.example.groovyjpa.groovyjpa`

## Important Changes

Note that the line

`annotationProcessor "io.micronaut.data:micronaut-data-processor"`

MUST be changed to 

`compileOnly "io.micronaut.data:micronaut-data-processor"`

as we are using Groovy.

## Maria DB

To see how the application would need to be modified to work with a MariaDB 
instance take a look at the `mariadb` branch.
