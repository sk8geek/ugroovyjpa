package uk.co.channele.example.groovyjpa

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification
import uk.co.channele.example.groovyjpa.domain.Student

import javax.inject.Inject

@MicronautTest
class StudentSpec extends Specification {

    @Inject
    @Client("/students")
    HttpClient client

    void "test"() {

        when: "get a list"
        def list = client.toBlocking().retrieve("/", Iterable.class)

        then: "list is empty"
        list.empty

        when: "add a student"
        def request = HttpRequest.POST("/", "Alice")
        client.toBlocking().exchange(request)

        and: "get the list again"
        list = client.toBlocking().retrieve("/", Iterable.class)

        then: "Alice exists"
        !list.empty
        list[0].name == "Alice"

    }

}
