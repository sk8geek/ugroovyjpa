package uk.co.channele.example.groovyjpa.controller

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import uk.co.channele.example.groovyjpa.domain.Student
import uk.co.channele.example.groovyjpa.repo.StudentRepo

import javax.inject.Inject

@Controller("/students/")
class StudentController {

    @Inject
    StudentRepo repo

    @Get
    HttpResponse<Iterable<Student>> index() {
        return HttpResponse.ok(repo.findAll())
    }

    @Post
    HttpResponse<?> add(@Body String name) {
        repo.save(new Student(name: name))
        return HttpResponse.noContent()
    }

}
