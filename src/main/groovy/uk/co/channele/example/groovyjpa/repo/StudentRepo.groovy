package uk.co.channele.example.groovyjpa.repo

import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository
import uk.co.channele.example.groovyjpa.domain.Student

@Repository
interface StudentRepo extends JpaRepository<Student, Long> {

}
